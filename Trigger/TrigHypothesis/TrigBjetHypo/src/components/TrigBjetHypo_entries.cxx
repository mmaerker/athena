
#include "TrigBjetHypo/TrigBjetHypoAllTE.h"
#include "TrigBjetHypo/TrigBjetHypo.h"
#include "TrigBjetHypo/TrigBjetFex.h"
#include "TrigBjetHypo/TrigBtagFex.h"
#include "TrigBjetHypo/TrigGSCFex.h"
#include "TrigBjetHypo/TrigLeptonJetFexAllTE.h"
#include "TrigBjetHypo/TrigLeptonJetMatchAllTE.h"
#include "TrigBjetHypo/TrigEFBjetSequenceAllTE.h"
#include "TrigBjetHypo/TrigJetSplitterAllTE.h"
#include "TrigBjetHypo/TrigSuperRoiBuilderAllTE.h"
#include "TrigBjetHypo/TrigBjetEtHypo.h"
#include "TrigBjetHypo/TrigFarawayJetFinderAllTE.h"

#include "../TrigBjetEtHypoAlgMT.h"
#include "../TrigBjetEtHypoAlgEVMT.h"
#include "../TrigBjetBtagHypoAlgMT.h"

#include "../TrigBjetHypoTool.h"
#include "../TrigBjetEtHypoTool.h"
#include "../TrigGSCFexMT.h"
#include "../TrigBtagFexMT.h"
#include "../TrigRoIFromJetsMT.h"
#include "../TrigJetSplitterMT.h"


DECLARE_COMPONENT( TrigBjetHypoAllTE )
DECLARE_COMPONENT( TrigBjetHypo )
DECLARE_COMPONENT( TrigBjetFex )
DECLARE_COMPONENT( TrigBtagFex )
DECLARE_COMPONENT( TrigGSCFex )
DECLARE_COMPONENT( TrigLeptonJetFexAllTE )
DECLARE_COMPONENT( TrigLeptonJetMatchAllTE )
DECLARE_COMPONENT( TrigEFBjetSequenceAllTE )
DECLARE_COMPONENT( TrigJetSplitterAllTE )
DECLARE_COMPONENT( TrigSuperRoiBuilderAllTE )
DECLARE_COMPONENT( TrigBjetEtHypo )
DECLARE_COMPONENT( TrigFarawayJetFinderAllTE )


DECLARE_COMPONENT( TrigBjetEtHypoAlgMT )
DECLARE_COMPONENT( TrigBjetEtHypoAlgEVMT )
DECLARE_COMPONENT( TrigBjetBtagHypoAlgMT )

DECLARE_COMPONENT( TrigBjetHypoTool )
DECLARE_COMPONENT( TrigBjetEtHypoTool )

DECLARE_COMPONENT( TrigGSCFexMT )
DECLARE_COMPONENT( TrigBtagFexMT )
DECLARE_COMPONENT( TrigRoIFromJetsMT )
DECLARE_COMPONENT( TrigJetSplitterMT )

