#include "tauRecTools/lwtnn/Exceptions.h"
#include <string>

namespace lwtDev {

  // ______________________________________________________________________
  // excpetions
  LightweightNNException::LightweightNNException(std::string problem):
    std::logic_error(problem)
  {}
  NNConfigurationException::NNConfigurationException(std::string problem):
    LightweightNNException(problem)
  {}
  NNEvaluationException::NNEvaluationException(std::string problem):
    LightweightNNException(problem)
  {}
  OutputRankException::OutputRankException(std::string problem):
    NNEvaluationException(problem)
  {}
}
