#include "EventBookkeeperTools/myCppFilterTest.h"
#include "../SkimDecisionMultiFilter.h"
#include "../EventCounterAlg.h"
#include "EventBookkeeperTools/BookkeeperTool.h"
#include "../StreamSelectorTool.h"
#include "../CutFlowSvc.h"

DECLARE_COMPONENT( myCppFilterTest )
DECLARE_COMPONENT( SkimDecisionMultiFilter )
DECLARE_COMPONENT( EventCounterAlg )
DECLARE_COMPONENT( BookkeeperTool )
DECLARE_COMPONENT( StreamSelectorTool )
DECLARE_COMPONENT( CutFlowSvc )

